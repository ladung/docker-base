FROM golang:alpine

RUN apk add git \
  && cd /tmp && wget https://github.com/upx/upx/releases/download/v3.95/upx-3.95-amd64_linux.tar.xz \
  && tar -xvf upx-3.95-amd64_linux.tar.xz \
  && mv /tmp/upx-3.95-amd64_linux/upx /usr/bin/

RUN mkdir /api

WORKDIR /src
